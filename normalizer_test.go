package searchQueryNormalizer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGASearchQueryDocumentFactory_NormalizeText(t *testing.T) {
	tests := map[string][]string{ // normalized_id: []variants
		"lego":              {"lego"},
		"duplolego":         {"lego duplo"},
		"180cmtyc":          {"tyč 180cm", "tyč 180 cm"},
		"2hipp":             {"hipp2", "hipp 2", " hipp   2 "},
		"23hipp":            {"hipp23", "hipp 23", "hipp   23"},
		"2bebe":             {"be2be", "be 2 be", "be 2be", "be2 be", "be  2   be", "  be  2   be "},
		"2sisasisa":         {"šíša2šíša"},
		"250sisasisa":       {"šíša250šíša", "šíša 250šíša", "šíša 250 šíša", "šíša250 šíša"},
		"250280sisasiso":    {"šíša250šíšo280", "šíša 250šíšo280", "šíša 250 šíšo280", "šíša 250 šíšo 280", "šíša250 šíšo280", "šíša250šíšo 280", "šíša  250 šíšo  280 ", "šíša280šíšo250", "šíša 280 šíšo 250", "šíša280 šíšo250", "šíša 280 šíšo250"},
		"250250sisasiso":    {"šíša250šíšo250", "šíša 250šíšo250", "šíša 250 šíšo250", "šíša 250 šíšo 250", "šíša250 šíšo250", "šíša250šíšo 250", "šíša  250 šíšo  250 "},
		"210250280sisasiso": {"210šíša250šíšo280", "210 šíša250šíšo280", "210 šíša250šíšo  280"},
		"ideapad":           {"ideaPad", "ideapad", "idea Pad"},
		"ideapadnotebooky":  {"notebooky ideaPad", "notebooky ideapad"},
		"babyborn":          {"BabyBorn", "Baby Born", "baby Born"},
		"2babyborn":         {"Baby2Born", "Baby 2 Born", "baby 2 Born"},
		"adcblackfire":      {"adcblackfire", "adc blackfire", "adc blackfire", "adc black fire"},
	}

	factory := NewSearchQueryNormalizer()
	for output, inputs := range tests {
		for _, input := range inputs {
			assert.Equal(t, output, factory.NormalizeText(input), "error: "+input)
		}
	}
}

func BenchmarkGASearchQueryDocumentFactory_NormalizeTextSplitOnNumbers(b *testing.B) {
	factory := NewSearchQueryNormalizer()
	for i := 0; i < b.N; i++ {
		_ = factory.NormalizeText("210 šíša250šíšo280")
	}
}

func BenchmarkGASearchQueryDocumentFactory_NormalizeTextNoNumbers(b *testing.B) {
	factory := NewSearchQueryNormalizer()
	for i := 0; i < b.N; i++ {
		_ = factory.NormalizeText("lego duplo")
	}
}
