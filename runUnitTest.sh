#!/bin/sh

set -e # exit immediately if a simple command exits with a nonzero exit value

GO_TEST_IMAGE=normalizer

# pre clean up
docker rm -f $(docker ps -qa) || true

# run go tests
docker build -t $GO_TEST_IMAGE -f ./docker/go/tests/Dockerfile .
docker run -i $GO_TEST_IMAGE

# post clean up
docker rm -f $(docker ps -qa) || true
