#!/bin/sh

set -e # exit immediately if a simple command exits with a nonzero exit value

GOAPI_LINT_IMAGE=normalizer

# clean up first
docker rm -f $(docker ps -qa) || true

docker build -t $GOAPI_LINT_IMAGE -f ./docker/go/lint/Dockerfile .
docker run -i $GOAPI_LINT_IMAGE

# post clean up
docker rm -f $(docker ps -qa) || true
