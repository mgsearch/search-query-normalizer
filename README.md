# Search Query Normalizer

Allows grouping of multiple search queries with the same meaning and creates normalized "fingerprint" of that text queries.
 
E.g. queries like `lego duplo`,`duplo lego`, `  Lego  duplo  ` and `LEGO Duplo` will get the same fingerprint. Also normalizes queries with numbers - query `door 80cm` and `door 80 cm` will have the same fingerprint (works the same as split_on_numerics in Elasticsearch).
