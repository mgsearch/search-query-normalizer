module gitlab.com/mgsearch/search-query-normalizer

go 1.15

require (
	github.com/blevesearch/bleve v1.0.14
	github.com/hkulekci/ascii-folding v0.0.0-20170317205928-8d72abd50aaf
	github.com/stretchr/testify v1.6.1
)
