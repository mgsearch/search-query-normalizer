package searchQueryNormalizer

import (
	"sort"
	"strings"
	"unicode"

	bleveUnicode "github.com/blevesearch/bleve/analysis/tokenizer/unicode"
	asciiFolding "github.com/hkulekci/ascii-folding"
)

type SearchQueryNormalizer struct {
}

func NewSearchQueryNormalizer() *SearchQueryNormalizer {
	return &SearchQueryNormalizer{}
}

/**
Creates normalized "fingerprint" from text query, e.g. queries like "lego duplo",
"duplo lego", " Lego  duplo" and "LEGO Duplo" will get the same fingerprint. Also works on queries
with numbers - query "door 80cm" and "door 80 cm" will have the same fingerprint (works
the same as split_on_numerics in Elasticsearch)
*/
func (factory *SearchQueryNormalizer) NormalizeText(query string) string {
	// split query to words
	words := strings.Split(query, " ")
	sanitizedWords := make([]string, 0)
	for _, word := range words {
		if strings.TrimSpace(word) == "" {
			// skip empty spaces or multiple spaces
			continue
		}
		if factory.stringContainsLettersAndNumbersCombination(word) {
			sanitizedWords = append(sanitizedWords, factory.splitTextToLettersAndNumbers(word)...)
		} else {
			sanitizedWords = append(sanitizedWords, word)
		}
	}

	// tokenize words based on bleve tokenizer
	normalized := make([]string, 0)
	tokens := bleveUnicode.NewUnicodeTokenizer().Tokenize([]byte(strings.Join(sanitizedWords, " ")))
	for _, token := range tokens {
		textAscii := strings.ToLower(asciiFolding.Fold(string(token.Term)))
		normalized = append(normalized, textAscii)
	}
	sort.Strings(normalized)
	normalizedString := strings.Join(normalized, "")

	return normalizedString
}

func (factory *SearchQueryNormalizer) stringContainsLettersAndNumbersCombination(text string) bool {
	letterExists := false
	numberExists := false

	for _, runeCharacter := range text {
		if unicode.IsLetter(runeCharacter) {
			letterExists = true
		}
		if unicode.IsNumber(runeCharacter) {
			numberExists = true
		}
		if letterExists && numberExists {
			return true
		}
	}

	return false
}

func (factory *SearchQueryNormalizer) splitTextToLettersAndNumbers(text string) []string {
	words := make([]string, 0)
	numbers := make([]string, 0)
	letters := ""
	numerals := ""

	// first split text to each rune (must be done separately at the first time to get exact rune count)
	runes := make([]rune, 0)
	for _, runeCharacter := range text {
		runes = append(runes, runeCharacter)
	}
	lastPosition := len(runes) - 1

	for position, runeCharacter := range runes {
		// append rune to either numbers or letters
		isNumber := unicode.IsNumber(runeCharacter)
		if isNumber {
			numerals += string(runeCharacter)
		} else {
			letters += string(runeCharacter)
		}
		isEnd := lastPosition == position
		if !isEnd {
			// check if next rune is also number or letter; if so - save and continue to the next chunk of text
			if !isNumber && unicode.IsNumber(runes[position+1]) {
				words = append(words, letters)
				letters = ""
			} else if isNumber && !unicode.IsNumber(runes[position+1]) {
				numbers = append(numbers, numerals)
				numerals = ""
			}
		} else {
			if len(letters) > 0 {
				words = append(words, letters)
			} else if len(numerals) > 0 {
				numbers = append(numbers, numerals)
			}
		}
	}

	return append(numbers, words...)
}
